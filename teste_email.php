<?php

include('index.html');

date_default_timezone_set('America/Sao_Paulo');
 
require_once('./src/PHPMailer.php');
require_once('./src/SMTP.php');
require_once('./src/Exception.php');
 
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;
 
if((isset($_POST['email']) && !empty(trim($_POST['email']))) && (isset($_POST['cod']) && !empty(trim($_POST['cod'])))) {
 
	$email = $_POST['email'];
	$mensagem = $_POST['cod'];
 
	$mail = new PHPMailer();
	$mail->isSMTP();
	$mail->Host = 'smtp.gmail.com';
	$mail->SMTPAuth = true;
	$mail->Username = 'seuemail@gmail.com';
	$mail->Password = 'suasenha';
	$mail->Port = 587;
 
	$mail->setFrom($email);
	$mail->addAddress('emaildodestinatario');
 
	$mail->isHTML(true);
	$mail->Subject = "Encomenda entregue! ";
    $mail->Body = " Isso é um teste <br>".
                   "Dados do envio <br> ". 
				   "Marc: Tiago Bicalho Franck <br>".
				   "Tel: 11 97266-4157 <br>".
				   "Travessa: R. Monsenhor Giovanni Batista Scalabrini <br>".
				   "Mauá,São Paulo";
				
	if($mail->send()) {
		echo 'Email enviado com sucesso.';
	} else {
		echo 'Email não enviado.';
	}
} else {
	echo 'Não enviado: informar o email e o código.';
}

?>